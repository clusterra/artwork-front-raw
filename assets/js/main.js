$(document).ready(function () {
    var openPhotoSwipe = function() {
        var pswpElement = document.querySelectorAll('.pswp')[0];
        
        // build items array
        var items = [
            {
                src: '../img/work.jpg',
                w: 881,
                h: 591
            }
        ];

        // define options (if needed)
        var options = {
                 // history & focus options are disabled on CodePen        
            history: false,
            focus: false,

            showAnimationDuration: 0,
            hideAnimationDuration: 0

        };

        var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };
    
    document.getElementById('work_pic').onclick = openPhotoSwipe;
    
    (function() {
			[].slice.call(document.querySelectorAll('.filters')).forEach(function(menu) {
				var menuItems = menu.querySelectorAll('.menu_link'),
					setCurrent = function(ev) {
						ev.preventDefault();

						var item = ev.target.parentNode; // li

						// return if already current
						if (classie.has(item, 'active')) {
							return false;
						}
						// remove current
						classie.remove(menu.querySelector('.active'), 'active');
						// set current
						classie.add(item, 'active');
					};

				[].slice.call(menuItems).forEach(function(el) {
					el.addEventListener('click', setCurrent);
				});
			});
    })(window);
    
    $('.task_text').editable({
            url: '/post',
            rows: 15,
            mode: 'inline',
            type: 'textarea'
    });
    
    $('.edit.btn').on('click', function(e){
        e.stopPropagation();
        $('.actions').css('display','none');
        $('.task_text').editable('show');
    });
    
    $('.editable-cancel, .editable-submit').on('click', function(){
       $('.actions').css('display','block'); 
    });
    
    $('.full-block').on('click', function(e){
        e.stopPropagation();
        $('.inform_block').toggleClass('show');
    });
            
    $("#comment_form").on("show.bs.collapse", function(){
        $(".add_mark a").css('opacity','0.4').attr('disable','true').addClass('disabled');
    });
    $('#comment_form button').on('click', function(){
       $('#comment_form').collapse("hide");
       $(".add_mark a").css('opacity','1').attr('disable','false').removeClass('disabled');
    });
	$('#work_upload').validate({
		messages: {
				work_descr: {
					required: 'Пожалуйста заполните это поле',
					minlength: 'Введите по меньшей мере {0} символов'
				}
		},
        submitHandler: function(form) {
				var bttn = document.getElementById( 'notification-trigger' );
                var work_form = document.getElementById( 'work_upload' );
				// make sure..
				bttn.disabled = false;
                    event.preventDefault();
					// simulate loading (for demo purposes only)
					classie.add( bttn, 'active' );
					setTimeout( function() {

						classie.remove( bttn, 'active' );
						
						// create notification
						var notification = new NotificationFx({
							message : '<i class="notify notify-megaphone"></i><p>Поздравляем! Ваша работа успешно загружена и доступна в списке работ.</p>',
							layout : 'bar',
							effect : 'slidetop',
							type : 'notice', // notice, warning or error
							onClose : function() {
								bttn.disabled = false; 
							}
						});
                        
                        /*// create warning
                        var notification = new NotificationFx({
							message : '<i class="notify notify-megaphone"></i><p>К сожалению, ваша работа не была загружена. Пожалуйста повторите попытку.</p>',
							layout : 'bar',
							effect : 'slidetop',
							type : 'warning', // notice, warning or error
							onClose : function() {
								bttn.disabled = false; 
							}
						});
                        
                        // create error
                        var notification = new NotificationFx({
							message : '<i class="notify notify-megaphone"></i><p>Упс! Что-то пошло не так. Приносим свои извинения.</p>',
							layout : 'bar',
							effect : 'slidetop',
							type : 'error', // notice, warning or error
							onClose : function() {
								bttn.disabled = false; 
							}
						});*/
                        $('#upload_work').modal('hide');
						// show the notification
						notification.show();

					}, 1200 );
					
					// disable the button (for demo purposes only)
					this.disabled = true;

        $(form).ajaxSubmit();
        }
	});
    $('#edit_work').on('show.bs.modal', function(e) {
        setTimeout(function () {
           p = planit.new({
              container: 'planit',
              image: {
                url: '../img/work.jpg'
              },
              /*markers: [
                {
                    coords: ['50', '50'],
                    draggable: false,
                    infobox: {
                        html: "<div class='tool_comment'><div class='tool_text'>Большое спасибо за комментарий. Конечно, я не совсем согласен, но есть довольно полезные советы и мысли.</div><div class='tool_author you'>Я</div></div><hr><div class='leave_comment'><form><textarea class='transparent_field' placeholder='Напишите комментарий'></textarea><button type='submit' class='button'>Добавить комментарий</button></form></div>",
                        position: 'right',
                        arrow: true
                    },
                    class: "mine"
                },
              ],*/
              markerMouseOver: function(event, marker){
                  marker.showInfobox();
                  marker.unhideInfobox();
              }
           });
            $('.planit-markers-container').on('click', function(e, marker){
            if(e.target != this) return false;
              var offset = $(this).offset();
              var coordX = (e.pageX - offset.left - 14) / $(this).width() * 100;
              var coordY = (e.pageY - offset.top - 14) / $(this).height() * 100;
            $('.planit-infobox').removeClass('active');
            $('.planit-marker').removeClass('new_marker');
              p.addMarker({
                coords: [coordX, coordY],
                draggable: false,
                infobox: {
                    html: "<span class='infobox_close' aria-hidden='true'>×</span><div class='clearfix'></div><div class='tool_comment'><div class='tool_text'>Нужно полностью поменять все шрифты. Один шрифт на весь лендинг это очень плохо. Выбранный шрифт с натяжкой подойдет только для заголовков. Основной текст слишком крупный. Плюс постоянные картинки на фонах тоже все очень ухудшают.</div><span class='pre_author'>&mdash;</span><div class='tool_author'>Артём Абзанов</div><span class='tip_date pull-right'>24 МАЯ В 23:15</span></div><hr><div class='tool_comment'><div class='tool_text'>Большое спасибо за комментарий. Конечно, я не совсем согласен, но есть довольно полезные советы и мысли.</div><span class='pre_author'>&mdash;</span><div class='tool_author you'>Я</div><span class='tip_date pull-right'>24 МАЯ В 23:15</span></div><hr><div class='leave_comment'><form><textarea class='transparent_field' placeholder='Напишите комментарий'></textarea><button type='submit' class='button'>Добавить комментарий</button></form></div>",
                    position: 'right',
                    arrow: true
                },
                class: "new_marker"
              });
              $('.new_marker').mouseover();
              $('.infobox_close').on('click', function(){
                $('.planit-infobox').removeClass('active');
              });
        });
           // $('.transparent_field').autoResize();
        }, 200); 
    });
    
    $(document.body)
        .on('show.bs.modal', function () {
            if (this.clientHeight <= window.innerHeight) {
                return;
            }
        
            var scrollbarWidth = getScrollBarWidth()
            if (scrollbarWidth) {
                $('.navbar-fixed-top').css('padding-right', scrollbarWidth);    
            }
        })
        .on('hidden.bs.modal', function () {
            $('.navbar-fixed-top').css('padding-right', 0);
        });

        function getScrollBarWidth () {
            var inner = document.createElement('p');
            inner.style.width = "100%";
            inner.style.height = "200px";

            var outer = document.createElement('div');
            outer.style.position = "absolute";
            outer.style.top = "0px";
            outer.style.left = "0px";
            outer.style.visibility = "hidden";
            outer.style.width = "200px";
            outer.style.height = "150px";
            outer.style.overflow = "hidden";
            outer.appendChild (inner);

            document.body.appendChild (outer);
            var w1 = inner.offsetWidth;
            outer.style.overflow = 'scroll';
            var w2 = inner.offsetWidth;
            if (w1 == w2) w2 = outer.clientWidth;

            document.body.removeChild (outer);

            return (w1 - w2);
        };
    
    

/*    $('.rating-tooltip-manual').rating({
      extendSymbol: function () {
        var title;
        $(this).tooltip({
          container: 'body',
          placement: 'bottom',
          trigger: 'manual',
          title: function () {
            return title;
          }
        });
      }
    });*/
    
    
    
    $('#user_info').popover({ 
          html: true,
          placement: 'bottom',
          delay: { 
             show: "65", 
             hide: "0"
          },
          content: function() {
            return $(this).parent().find('.tooltip_text').html();
          }
    });
    
    $("div#dropzone").dropzone({ 
        url: "/file/post",
        paramName: "work",
        maxFilesize: 2, 
        clickable: true
    });
    
    
    
     $(function(){
      $(".increment").click(function(){
        var count = parseInt($("~ .count", this).text());

        if($(this).hasClass("up")) {
          var count = count + 1;

           $("~ .count", this).text(count);
        } else {
          var count = count - 1;
           $("~ .count", this).text(count);     
        }

        $(this).parent().addClass("bump");

        setTimeout(function(){
          $(this).parent().removeClass("bump");    
        }, 400);
      });
    });
    
    
    
    /*var elem = document.querySelector('.js-switch');
    var switchery = new Switchery(elem, { size: 'small', color: '#139edc' });*/
    
    
        var _renderItem = function(data) {
		  return "<p>Title :" + data.title + "</p>"
	    }
    
        $('#work_container').infinitescroll({
            loading: {
                finishedMsg: "<p class='text-center'>На данный момент вы видите весь список работ</p>",
                msgText: "<p class='text-center'>Загружаем работы...</p>",
                speed: 'medium',
                img: '../img/loader.svg'
            },
            navSelector  	: "#next:last",
            nextSelector 	: "a#next:last",
            itemSelector 	: "#work_container .col-lg-3.col-md-3.col-sm-6.col-xs-12",
            debug		 	: true,
            dataType	 	: 'json',
            appendCallback	: false
        }, function( response ) {
            var jsonData = response.results;
                $theCntr = $("#work_container");
                var newElements = "";
                //var newItems = new Array();
                for(var i=0;i<jsonData.length;i++) {
                    var item = $(_renderItem(jsonData[i]));
                    //item.css({ opacity: 0 });
                    $theCntr.append(item);
                    //newItems.push(item.attr('id'));
                }
                //_addMasonryItem(newItems);
        });

});



	function isIOSSafari() {
		var userAgent;
		userAgent = window.navigator.userAgent;
		return userAgent.match(/iPad/i) || userAgent.match(/iPhone/i);
	};

	function isTouch() {
		var isIETouch;
		isIETouch = navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
		return [].indexOf.call(window, 'ontouchstart') >= 0 || isIETouch;
	};

	var isIOS = isIOSSafari(),
		clickHandler = isIOS || isTouch() ? 'touchstart' : 'click';

	function extend( a, b ) {
		for( var key in b ) { 
			if( b.hasOwnProperty( key ) ) {
				a[key] = b[key];
			}
		}
		return a;
	}

	function Animocon(el, options) {
		this.el = el;
		this.options = extend( {}, this.options );
		extend( this.options, options );

		this.checked = false;

		this.timeline = new mojs.Timeline();
		
		for(var i = 0, len = this.options.tweens.length; i < len; ++i) {
			this.timeline.add(this.options.tweens[i]);
		}

		var self = this;
		this.el.addEventListener(clickHandler, function() {
			if( self.checked ) {
				self.options.onUnCheck();
			}
			else {
				self.options.onCheck();
				self.timeline.start();
			}
			self.checked = !self.checked;
		});
	}

    Animocon.prototype.options = {
		tweens : [
			new mojs.Burst({
				shape : 'circle',
				isRunLess: true
			})
		],
		onCheck : function() { return false; },
		onUnCheck : function() { return false; }
	};

		var el14 = document.querySelector('.icobutton'), el14span = el14.querySelector('span'), el14counter = el14.querySelector('span.icobutton__text');
		new Animocon(el14, {
			tweens : [
				// ring animation
				new mojs.Transit({
					parent: el14,
					duration: 750,
					type: 'circle',
					radius: {0: 28},
					fill: 'transparent',
					stroke: '#d8536f',
					strokeWidth: {22:0},
					opacity: 0.2,
					x: '45%',     
					y: '45%',
					isRunLess: true,
					easing: mojs.easing.bezier(0, 1, 0.5, 1)
				}),
				new mojs.Transit({
					parent: el14,
					duration: 500,
					delay: 60,
					type: 'circle',
					radius: {0: 20},
					fill: 'transparent',
					stroke: '#d8536f',
					strokeWidth: {5:0},
					opacity: 0.2,
					x: '50%', 
					y: '50%',
					shiftX : 10, 
					shiftY : -10,
					isRunLess: true,
					easing: mojs.easing.sin.out
				}),

				new mojs.Transit({
					parent: el14,
					duration: 800,
					delay: 120,
					type: 'circle',
					radius: {0: 20},
					fill: 'transparent',
					stroke: '#d8536f',
					strokeWidth: {5:0},
					opacity: 0.3,
					x: '50%', 
					y: '50%',
					shiftX : -10, 
					shiftY : 10,
					isRunLess: true,
					easing: mojs.easing.sin.out
				}),
				// icon scale animation
				new mojs.Tween({
					duration : 1200,
					easing: mojs.easing.ease.out,
					onUpdate: function(progress) {
						if(progress > 0.3) {
							var elasticOutProgress = mojs.easing.elastic.out(1.43*progress-0.43);
							el14span.style.WebkitTransform = el14span.style.transform = 'scale3d(' + elasticOutProgress + ',' + elasticOutProgress + ',1)';
						}
						else {
							el14span.style.WebkitTransform = el14span.style.transform = 'scale3d(0,0,1)';
						}
					}
				})
			],
			onCheck : function() {
				el14.style.color = '#d8536f';
				el14counter.innerHTML = Number(el14counter.innerHTML) + 1;
                el14counter.style.color = '#d8536f'
			},
			onUnCheck : function() {
				el14.style.color = '#AEBECA';
				var current = Number(el14counter.innerHTML);
				el14counter.innerHTML = current > 1 ? Number(el14counter.innerHTML) - 1 : '';
                el14counter.style.color = '#aebeca'
			}
		});


