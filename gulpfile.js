var lr = require('tiny-lr'), // Минивебсервер для livereload
    gulp = require('gulp'), // Сообственно Gulp JS
    jade = require('gulp-jade'), // Плагин для Jade
    stylus = require('gulp-stylus'), // Плагин для Stylus
    livereload = require('gulp-livereload'), // Livereload для Gulp
    myth = require('gulp-myth'), // Плагин для Myth - http://www.myth.io/
    csso = require('gulp-csso'), // Минификация CSS
    imagemin = require('gulp-imagemin'), // Минификация изображений
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    connect = require('connect'), // Webserver
    serveStatic = require('serve-static'), //Подключение модуля serveStatic для сервера
    server = lr();


// Собираем html из Jade
gulp.task('jade', function() {
    gulp.src(['./assets/template/*.jade', '!./assets/template/_*.jade'])
        .pipe(jade({
            pretty: true
        }))  // Собираем Jade только в папке ./assets/template/ исключая файлы с _*
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
    .pipe(gulp.dest('./public/')) // Записываем собранные файлы
    .pipe(livereload(server)); // даем команду на перезагрузку страницы
}); 


// Собираем Stylus
gulp.task('stylus', function() {
    gulp.src([
     './assets/stylus/*.styl',
     './assets/stylus/plugins/**', 
     './assets/stylus/responsive.css'
    ])
    .pipe(stylus())
    .on('error', console.log) // Если есть ошибки, выводим и продолжаем
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./public/css/')) // записываем css
    .pipe(livereload(server)); // даем команду на перезагрузку css
});


// Собираем JS
gulp.task('js', function() {
    gulp.src(['./assets/js/**/!(main)*.js', './assets/js/**/main.js'])
        .pipe(concat('app.js')) // Собираем все JS
        .pipe(gulp.dest('./public/js'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
});



// Копируем и минимизируем изображения
gulp.task('images', function() {
    gulp.src('./assets/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./public/img'))

});

// Копируем фавиконы
gulp.task('favicons', function() {
    gulp.src(['./assets/*', '!./assets/stylus/**', '!./assets/template/**'])
        .pipe(gulp.dest('./public'))

});

// Копируем шрифты

gulp.task('fonts', function() {
    gulp.src('./assets/fonts/**/**')
        .pipe(gulp.dest('./public/fonts'));
});


// Локальный сервер для разработки
gulp.task('http-server', function() {
    connect()
        .use(require('connect-livereload')())
        .use(serveStatic('./public'))
        .listen('9000');

    console.log('Server listening on http://localhost:9000');
});


// Запуск сервера разработки gulp watch
gulp.task('watch', function() {
    // Предварительная сборка проекта
    gulp.run('stylus');
    gulp.run('jade');
    gulp.run('images');
    gulp.run('favicons'); 
    gulp.run('fonts');
    gulp.run('js');

    // Подключаем Livereload
    server.listen(35729, function(err) {
        if (err) return console.log(err);

        gulp.watch('assets/stylus/**/**', function() {
            gulp.run('stylus');
        });
        gulp.watch('assets/template/**/*.jade', function() {
            gulp.run('jade');
        });
        gulp.watch('assets/img/**/*', function() {
            gulp.run('images');
        });
        gulp.watch('assets/*', function() {
            gulp.run('favicons');
        });
        gulp.watch('assets/fonts/**/**', function() {
            gulp.run('fonts');
        });
        gulp.watch('assets/js/**/*', function() {
            gulp.run('js');
        });
    });
    gulp.run('http-server');
});

// Сборка проекта
gulp.task('build', function() {
    // stylus
    gulp.src('./assets/stylus/**')
    .pipe(stylus())
    .on('error', console.log) // Если есть ошибки, выводим и продолжаем
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./build/css/')) // записываем css
    
    // jade
    gulp.src(['./assets/template/*.jade', '!./assets/template/_*.jade'])
        .pipe(jade({
            pretty: true
        }))  // Собираем Jade только в папке ./assets/template/ 
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
    .pipe(gulp.dest('./build/')) // Записываем собранные файлы

    // js
    gulp.src(['./assets/js/**/!(main)*.js', './assets/js/**/main.js'])
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build/js'));

    // image
    gulp.src('./assets/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./build/img'));
    
    // favicons
    gulp.src(['./assets/*', '!./assets/stylus/**', '!./assets/template/**'])
        .pipe(gulp.dest('./build'))
    
    // fonts
    gulp.src('./assets/fonts/**/**')
        .pipe(gulp.dest('./build/fonts'))

});